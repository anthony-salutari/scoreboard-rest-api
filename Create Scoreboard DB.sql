-- Create the database if it doesn't already exist --
CREATE DATABASE IF NOT EXISTS Scoreboard;

-- Make sure we're using the right schema --
USE Scoreboard;

-- Create the Scores table if it doesn't already exist --
CREATE TABLE IF NOT EXISTS Scores
(
	Id int PRIMARY KEY NOT NULL AUTO_INCREMENT, -- Auto incremented primary key --
    Player VARCHAR(50), -- Max 50 character player name that is required --
    Score BIGINT NOT NULL DEFAULT 0 -- bigint supports 2^64-1 points, is required, and defaults to 0 --
);

-- Add a few dummy rows --
INSERT INTO Scores (Player, Score) VALUES ('Antonio', 1000000);
INSERT INTO Scores (Player, Score) VALUES ('Bailey', 575000);
INSERT INTO Scores (Player, Score) VALUES ('Django', -200);
INSERT INTO Scores (Player, Score) VALUES ('Pueblo', 9000);