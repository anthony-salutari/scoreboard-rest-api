// Boilerplate express setup
var express = require('express');
var app = express();

// Enable the status monitor
app.use(require('express-status-monitor')());

// Enable .env files
require('dotenv').config()

// Enable the json parsing middleware
app.use(express.json());

const mysql = require('mysql2');

// Create the connection to database
const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
});

/*
    GET
    Get the number 1 high score
    Returns the top score
*/
app.get('/highscore', (req, res) => {
    let sqlString = 'SELECT * FROM Scores ORDER BY Score DESC LIMIT 1';

    connection.query(
        sqlString,
        (err, results) => {
            if (err) {
                console.log(err);
                res.send(err);
            } else {
                res.send(results[0]);
            }
        }
    );
});

/*
    GET 
    Get the top 10 scores
    Returns a Json array of score objects
*/
app.get('/top10', (req, res) => {
    // create a new SQL string for the query we want to execute
    let sqlString = 'SELECT * FROM Scores ORDER BY Score DESC LIMIT 10';

    // call a query on the connection we created earlier and give it the SQL string
    connection.query(
        sqlString,
        (err, results) => {
            if (err) {
                console.log(err);
                res.send(err);
            } else {
                res.send(results);
            }
        }
    );
});

/*
    GET
    Get the top 25 scores
    Returns a Json array of score objects
*/
app.get('/top25', (req, res) => {
    let sqlString = 'SELECT * FROM Scores ORDER BY Score DESC LIMIT 25';

    connection.query(
        sqlString,
        (err, results) => {
            if (err) {
                console.log(err);
                res.send(err);
            } else {
                res.send(results);
            }
        }
    );
});

/*
    POST
    Add a new score
    Body Params: player, score
    Returns: 200 if successful and the error if not
*/
app.post('/add', (req, res) => {

    if (req.body.player === null || req.body.score === null)
    {
        res.sendStatus(400);
    }

    let player = req.body.player;
    let score = req.body.score;

    let sqlString = `INSERT INTO Scores (player, score) VALUES ('${player}', ${score})`;

    connection.query(
        sqlString,
        (err, results) => {
            if (err) {
                console.log(err);
                res.send(err);
            } else {
                console.log('New score added');
                res.sendStatus(200);
            }
        }
    );
});

/*
    PUT
    Update a score by id
    Route Params: id
    Body Params: player, score
    Returns: Ok if successful and the error if not
*/
app.put('/edit/:id', (req, res) => {
    let player = req.body.player;
    let score = req.body.score;

    let sqlString = `UPDATE Scores SET Player = '${player}', Score = ${score} WHERE Id = ${req.params.id}`;

    connection.query(
        sqlString,
        (err, results) => {
            if (err) {
                console.log(err);
                res.send(err);
            } else {
                console.log('Score updated');
                res.sendStatus(200);
            }
        }
    );
});

/*
    DELETE
    Delete a score by id
    Route Params: id
    Returns: 200 if successful and the error if not
*/
app.delete('/delete/:id', (req, res) => {
    let sqlString = `DELETE FROM Scores WHERE Id = ${req.params.id}`;

    connection.query(
        sqlString,
        (err, results) => {
            if (err) {
                console.log(err);
                res.send(err);
            } else {
                console.log(`Score ${req.params.id} deleted`);
                res.sendStatus(200);
            }
        }
    );
});

// Start the listen loop
var server = app.listen(8080, function () {  
    console.log(`Server listening at http://localhost:${server.address().port}`);
 });